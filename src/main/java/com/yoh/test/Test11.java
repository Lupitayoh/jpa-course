package com.yoh.test;
import com.yoh.domain.Tramite;
import com.yoh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Tuple;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
public class Test11 {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            /** this is with Criteria builder*/
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Tuple> criteria = builder.createQuery(Tuple.class);

            Root<Tramite> root = criteria.from(Tramite.class);

            Path<Integer> idTramPath = root.get("idTram");
            Path<Timestamp> fhcTramPath = root.get("fhcTram");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parseDate = dateFormat.parse("2018-03-30 12:52:57.0");

            criteria.multiselect(idTramPath, fhcTramPath)
                    .where(builder.and(
                            builder.like(root.<String>get("tipoTram"), "%credito%"),
                            builder.lessThan(root.<Timestamp>get("fhcTram"), new Timestamp(parseDate.getTime()))
                    ));

            List<Tuple> tuples = session.createQuery(criteria).getResultList();
            for(Tuple tuple : tuples){
                System.out.println(tuple.get(idTramPath) + ", " + tuple.get(fhcTramPath) );
            }

            tx.commit();
        } catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
