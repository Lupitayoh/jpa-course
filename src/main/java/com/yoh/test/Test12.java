package com.yoh.test;
import com.yoh.domain.Avaluo;
import com.yoh.domain.Tramite;
import com.yoh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;



public class Test12 {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

           /** guardar un tramite, un avaluo asignandole el tramite
           Tramite tramite = new Tramite("Credito Rechazado", new Timestamp(new Date().getTime()));
            session.save(tramite);

            Avaluo avaluo = new Avaluo("Hidalgo #102");
            avaluo.setTramite(tramite);
            session.save(avaluo);*/

           /**carga tramite existente*/
            Tramite tramite2 = session.load(Tramite.class, 8);
            Avaluo avaluo2 = new Avaluo("norte 5");
            avaluo2.setTramite(tramite2);
            session.save(avaluo2);

            tx.commit();
        } catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
