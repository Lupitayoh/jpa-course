package com.yoh.test;

import com.yoh.domain.Tramite;
import com.yoh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Test {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            /** this is with Criteria builder*/
            CriteriaBuilder builder = session.getCriteriaBuilder();

            CriteriaQuery<Tramite> criteria = builder.createQuery(Tramite.class);
            Root<Tramite> root = criteria.from(Tramite.class);

            criteria.select(root);
            criteria.where(builder.equal(root.get("tipoTram"), "creditos"));

            /** Espera una lista de tramites
            List<Tramite> tramites = session.createQuery(criteria).getResultList();*/
            /** Espera solo una respuesta*/
            Tramite tramite = session.createQuery(criteria).getSingleResult();

            // Actualizar el estado del tramite
            tramite.setTipoTram("Otro credito");

            session.update(tramite);

            /** Get transaction
            session.getTransaction();*/
            tx.commit();
        } catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        /**Funcion Save chida
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date = new Date();

        //crear instacia de tramite
        Tramite tramite = new Tramite("Avaluo", new Timestamp(date.getTime()));

        //Salvar el tramite en BD
        session.save(tramite); //insert into Tramite (tipoTram, fhcTram) values (?, ?)
         //**Funcion Save chida*/

        /** Get all list with HQL
        @SuppressWarnings("unchecked")
        Query<Tramite> query = session.createQuery("from Tramite where tipoTram = :tipoTram");
        query.setParameter("tipoTram", "Credito");
        List<Tramite> tramites = query.getResultList();
        System.out.println(tramites.toString());*/


    }
}
