package com.yoh.test;
import com.yoh.domain.Avaluo;
import com.yoh.domain.Tramite;
import com.yoh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class Test13 {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            //Consultar el trámite de un avaluo
            Avaluo avaluo = session.load(Avaluo.class, 2);
            Tramite tramite = avaluo.getTramite();
            System.out.println("Tramite del avalup 2 "+ tramite);
            // Todos los tramites que aparecen en avaluos
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Avaluo> criteria = builder.createQuery(Avaluo.class);
            Root<Avaluo> root = criteria.from(Avaluo.class);
            criteria.select(root);

            List<Avaluo> avaluos = session.createQuery(criteria).getResultList();
            System.out.println("Todos los tramites contenidos en avaluos");
            for (Avaluo avaluo2 : avaluos){
                System.out.println(avaluo2);
            }

            tx.commit();
        } catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
