package com.yoh.test;

import com.yoh.domain.Tramite;
import com.yoh.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Test10 {

    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();

            /** this is with Criteria builder*/
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Tramite> criteria = builder.createQuery(Tramite.class);

            Root<Tramite> root = criteria.from(Tramite.class);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parseDate = dateFormat.parse("2018-03-30 12:52:57.0");
            criteria.select(root)
                    .where(builder.and(
                            builder.like(root.<String>get("tipoTram"), "%credito%"),
                            builder.lessThan(root.<Timestamp>get("fhcTram"), new Timestamp(parseDate.getTime()))
                    ));

            List<Tramite> tramites = session.createQuery(criteria).getResultList();
            System.out.println(tramites.toString());

            tx.commit();
        } catch (Exception e){
            if(tx != null){
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }
}
