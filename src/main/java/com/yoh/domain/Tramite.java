package com.yoh.domain;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(name = "Tramite")
public class Tramite {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idTram;

    @Column(name = "tipoTram")
    private String tipoTram;
    private Timestamp fhcTram;

    @OneToOne(mappedBy = "tramite")
    private Avaluo avaluo;

    @Override
    public String toString() {
        return "Tramite{" +
                "idTram=" + idTram +
                ", tipoTram='" + tipoTram + '\'' +
                ", fhcTram=" + fhcTram +
                '}';
    }

    public Tramite(){}

    public Tramite(String tipoTram, Timestamp fhcTram) {
        this.tipoTram = tipoTram;
        this.fhcTram = fhcTram;
    }

    public int getIdTram() {
        return idTram;
    }

    public void setIdTram(int idTram) {
        this.idTram = idTram;
    }

    public String getTipoTram() {
        return tipoTram;
    }

    public void setTipoTram(String tipoTram) {
        this.tipoTram = tipoTram;
    }

    public Timestamp getFhcTram() {
        return fhcTram;
    }

    public void setFhcTram(Timestamp fhcTram) {
        this.fhcTram = fhcTram;
    }
}
